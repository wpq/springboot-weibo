package com.lagou.weibo.controller;

import com.lagou.weibo.service.IArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.Calendar;

@Controller
public class IndexController {

    @Autowired
    IArticleService articleService;

    @RequestMapping("/index")
    public String index(Model model, HttpServletRequest request){
        String currentPage = request.getParameter("currentPage");
        String pageSize = request.getParameter("pageSize");
        model.addAttribute("page", articleService.getArticleByPage(
                currentPage == null ? 1: Integer.parseInt(currentPage),
                pageSize == null ? 3: Integer.parseInt(pageSize)));

        model.addAttribute("currentYear", Calendar.getInstance().get(Calendar.YEAR));
        return "index";
    }
}
