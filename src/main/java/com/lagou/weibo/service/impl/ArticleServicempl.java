package com.lagou.weibo.service.impl;

import com.lagou.weibo.entity.Article;
import com.lagou.weibo.mapper.ArticleMapper;
import com.lagou.weibo.service.IArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ArticleServicempl implements IArticleService {

    @Autowired
    ArticleMapper articleMapper;

    @Override
    public Map<String, Object> getArticleByPage(Integer currentPage, Integer pageSize){
        List<Article> articleList = articleMapper.getArticleForPage((currentPage-1) * pageSize, pageSize);
        long count = articleMapper.count();
        long pages = count%pageSize==0 ? count/pageSize : (long) (Math.floor(count / pageSize) + 1);

        Map<String, Object> result = new HashMap<>();
        result.put("currentPage", currentPage);
        result.put("pageSize", currentPage);
        result.put("articleList", articleList);
        result.put("count", count);
        result.put("previousPageable", currentPage-1);
        result.put("nextPageable", currentPage+1);


        result.put("pages", pages);
        return result;
    };


}
