package com.lagou.weibo.service;

import java.util.Map;

public interface IArticleService {

    Map<String, Object> getArticleByPage(Integer currentPage, Integer pageSize);
}
