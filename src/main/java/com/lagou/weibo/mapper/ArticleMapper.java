package com.lagou.weibo.mapper;


import com.lagou.weibo.entity.Article;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ArticleMapper {

    //根据id查询对应的文章
    public Article selectArticle(Integer id);

    public long count();

    public List<Article> getArticleForPage(@Param("current") Integer current,
                                           @Param("pageSize")Integer pageSize);

}
