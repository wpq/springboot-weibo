package com.lagou.weibo;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@MapperScan("com.lagou.weibo.mapper")
@SpringBootApplication
public class SpringbootWeiboApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootWeiboApplication.class, args);
    }

}
